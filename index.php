<?php
/**
 * Get UrbanDictionary definitions on Whatsapp. Powered by Twilio API for WhatsApp
 *
 * @author  Michael Okoko <michaels@mchl.xyz>
 * @license https://opensource.org/licenses/MIT MIT
 */

require __DIR__ . '/vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$router = new \Bramus\Router\Router();

$router->post(
    "receive-message", function () {
        // the Body parameter contains the message body sent to us from WhatsApp
        $term = $_REQUEST['Body'];
        // the From variable is similar to "whatsapp%3A%2B2349074919559"
        $sender = $_REQUEST['From'];

        $response = getDefinitionsFor($term);
        $definitions = json_decode($response);
        if (count($definitions->list) < 1) {
            $top = $definitions->list[0];
            $message = $top->definition;
            $message .= "\n\n";
            $message .= "See https://www.urbandictionary.com/define.php?term=";
            $message .= urlencode($term);

        } else {
            $message = "No definition was found for ".$term;
        }

        sendMessage($message, $sender);

        header("Content-type", "application/json");
        echo json_encode(["status" => "OK"]);
        exit(0);
    }
);

/**
 * Fetch the definition of $term from the UD API
 * 
 * @param $term string Term to search for
 * 
 * @return string
 */
function getDefinitionsFor($term) 
{
    $options = [
        CURLOPT_URL => "http://api.urbandictionary.com/v0/define?term=".$term,
        CURLOPT_RETURNTRANSFER => true
    ];
    $handle = curl_init();
    curl_setopt_array($handle, $options);
    $response = curl_exec($handle);
    curl_close($handle);
    
    return $response;
}

/**
 * Sends $message to $recipient on WhatsApp
 * 
 * @param $message   string Message to sent
 * @param $recipient string Twilio-formatted phone number of recipient
 * 
 * @return void
 */
function sendMessage($message, $recipient)
{
    $sid = $_ENV["TWILIO_ACCOUNT_SID"];
    $token = $_ENV["TWILIO_AUTH_TOKEN"];
    $twilio = new \Twilio\Rest\Client($sid, $token);
    $twilio->messages->create(
        $recipient, [
            "from" => "whatsapp:".$_ENV["TWILIO_WHATSAPP_NUMBER"],
            "body" => $message
        ]
    );

    return;
}

$router->run();
