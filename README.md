# UrbanDictionBot
<img src="./assets/screenshot.png" alt="drawing" width="300"/>

Confused by a hip word while texting? Hit the bot up with the word
and it will send you the UrbanDictionary definition (if it exists).

## How it works
### Stack
- [Bref PHP](https://bref.sh)
- [AWS Lambda](https://aws.amazon.com/lambda/) and the [Serverless Framework](http://serverless.com/)
- [The Twilio API for WhatsApp](https://www.twilio.com/whatsapp)
- [The Urban Dictionary API](api.urbandictionary.com)
### Implementation
When you message the bot, The Twilio WhatsApp API triggers the webhook at
`/receive-message`. 

The payload sent to the webhook contains the term you sent
as well as your WhatsApp phone number.
The bot then queries the UrbanDictionary for your word and if it finds it,
picks the definition with the most votes and send it back to you.
### Deployment
Some resources on deploying Bref apps to Lambda are:
- [Bref Documentation](https://bref.sh/docs/deploy.html)
- [Article on deploying a sample Bref application](https://www.twilio.com/blog/verify-phone-numbers-with-bref-php-twilio-verify)